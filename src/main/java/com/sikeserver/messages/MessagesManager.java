package com.sikeserver.messages;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import com.sikeserver.core.CoreManager;
import com.sikeserver.messages.listener.*;

public class MessagesManager extends JavaPlugin {
	public CoreManager core;
	public static MessagesManager plugin;

	/**
	 * プラグインが有効化されたときに呼び出されるイベント。
	 * 
	 * @author Siketyan
	 */
	@Override
	public void onEnable() {
		// CoreプラグインのCoreManagerクラスを取得
		core = CoreManager.getPlugin();
		// pluginにこのクラスを代入
		plugin = this;

		PluginManager manager = Bukkit.getServer().getPluginManager();
		manager.registerEvents(new ChangeJoinMessage(), this);
		manager.registerEvents(new ChangeLeaveMessage(), this);
		manager.registerEvents(new ChangeDeathMessage(), this);
		manager.registerEvents(new ChangeMotd(), this);
	}

	/**
	 * MessagesManagerを取得します。
	 * 
	 * @author Siketyan
	 * 
	 * @return そのままポイッ！
	 */
	public static MessagesManager getPlugin() {
		return plugin;
	}
}
