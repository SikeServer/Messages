package com.sikeserver.messages.listener;

import org.bukkit.entity.Arrow;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;

import com.sikeserver.core.CoreManager;
import com.sikeserver.messages.MessagesManager;

public class ChangeDeathMessage implements Listener {
	private MessagesManager plugin;
	private CoreManager core;

	public ChangeDeathMessage() {
		plugin = MessagesManager.getPlugin();
		core = plugin.core;
	}

	@EventHandler
	public void onPlayerDeath(EntityDamageEvent event) {
		if (event.getEntityType() != EntityType.PLAYER) {// プレイヤーではない
			return;
		}
		Player p = (Player) event.getEntity();
		if (Double.compare(p.getHealth(), event.getFinalDamage()) > 0) {// HPが0未満(p.isDead()はTickが進んでいないため正常に機能しない)
			return;
		}
		String msg;
		if (event instanceof EntityDamageByEntityEvent) {
			try {
				if (((EntityDamageByEntityEvent) event).getDamager().getName().equals("Arrow")) {
					Arrow a = (Arrow)((EntityDamageByEntityEvent) event).getDamager();
					if (a.getShooter() instanceof Player) {
						msg = core.getMessage("Message.Death.Entity.PLAYER_ARROW")
								.replaceAll("%damager%", ((Player)a.getShooter()).getName())
								.replaceAll("%player%", p.getName());
					} else {
						msg = core.getMessage("Message.Death.Entity.SKELETON")
								.replaceAll("%player%", p.getName());
					}
				} else if (((EntityDamageByEntityEvent) event).getDamager() instanceof Player) {
					msg = core.getMessage("Message.Death.Entity.PLAYER")
							.replaceAll("%damager%", ((EntityDamageByEntityEvent) event).getDamager().getName())
							.replaceAll("%player%", p.getName());
				} else {
					msg = core
						.getMessage("Message.Death.Entity."
							+ ((EntityDamageByEntityEvent) event).getDamager().getType().toString())
						.replaceAll("%damager%", ((EntityDamageByEntityEvent) event).getDamager().getName())
						.replaceAll("%player%", p.getName());
				}
			} catch (Exception e) // ymlに記述がなかった時の対策
			{
				e.printStackTrace();
				msg = core.getMessage("Message.Death.Entity.UNKNOWN")
						.replaceAll("%damager%", ((EntityDamageByEntityEvent) event).getDamager().getName())
						.replaceAll("%player%", p.getName());
			}
		} else {
			try {
				msg = core.getMessage("Message.Death.Other." + event.getCause()).replaceAll("%player%",
						p.getName());
			} catch (Exception e) // ymlに記述がなかった時の対策
			{
				msg = core.getMessage("Message.Death.Other.UNKNOWN").replaceAll("%player%",
						p.getName());
			}
		}

		plugin.getServer().broadcastMessage(msg);
	}

	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent event) {
		// ブロードキャストするのでデフォルトの死亡メッセージは不要
		event.setDeathMessage(null);
	}
}
