package com.sikeserver.messages.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import com.sikeserver.core.CoreManager;
import com.sikeserver.messages.MessagesManager;

public class ChangeLeaveMessage implements Listener {
	private MessagesManager plugin;
	private CoreManager core;

	public ChangeLeaveMessage() {
		plugin = MessagesManager.getPlugin();
		core = plugin.core;
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onPlayerLeave(PlayerQuitEvent event) {
		String msg = core.getMessage("Message.Event.Leave").replaceAll("%player%", event.getPlayer().getName());
		event.setQuitMessage(msg);
	}
}
