package com.sikeserver.messages.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;

import com.sikeserver.core.CoreManager;
import com.sikeserver.messages.MessagesManager;

public class ChangeMotd implements Listener {
	private MessagesManager plugin;
	private CoreManager core;

	public ChangeMotd() {
		plugin = MessagesManager.getPlugin();
		core = plugin.core;
	}

	@EventHandler
	public void onServerListPing(ServerListPingEvent event) {
		String Motd = core.getMessage("Motd");
		event.setMotd(Motd);
	}
}
