package com.sikeserver.messages.listener;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import com.sikeserver.auth.util.AuthCode;
import com.sikeserver.core.CoreManager;
import com.sikeserver.core.util.DateTime;
import com.sikeserver.core.util.SQLManager;
import com.sikeserver.messages.MessagesManager;

public class ChangeJoinMessage implements Listener {
	private MessagesManager plugin;
	private CoreManager core;
	private SQLManager sql;

	private List<String> welcome;

	public ChangeJoinMessage() {
		plugin = MessagesManager.getPlugin();
		core = plugin.core;
		sql = core.sql;
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerJoin(PlayerJoinEvent event) {
		Player p = event.getPlayer();
		
		welcome = core.getMessageList("Message.Event.Welcome");
		for (String oneLine : welcome.toArray(new String[welcome.size()])) {
			String msg = ChatColor.translateAlternateColorCodes('&', oneLine);
			p.sendMessage(msg);
		}

		try {
			if (isNewPlayer(p)) {
				try {
					sql.executeUpdate(
						"INSERT INTO " + CoreManager.playerTable
							+ "(id, mcid, uuid, lastlogin, money, authcode) VALUES(NULL, \""
							+ p.getName() + "\", \""
							+ p.getUniqueId() + "\", \""
							+ DateTime.getDateTime("yyyy/MM/dd HH:mm") + "\", 1000000, \""
							+ AuthCode.generateAuthCode() + "\")"
					);
					plugin.getLogger().info("Registed user : "
						+ p.getName() + " ("
						+ p.getUniqueId() + ")");

					String msg = core.getMessage("Message.Event.NewJoin")
						.replaceAll("%player%", p.getName());
					event.setJoinMessage(msg);
				} catch (SQLException e) {
					plugin.getLogger().warning("Error! Failed adding new player...");
					p.kickPlayer(core.getMessage("Error.Messages.NewJoin"));
					e.printStackTrace();
					return;
				}
				
				p.teleport(p.getWorld().getSpawnLocation());
			} else {
				try {
					sql.executeUpdate(
						"UPDATE " + CoreManager.playerTable + " SET mcid = \""
							+ p.getName()
							+ "\", lastlogin = \"" + DateTime.getDateTime("yyyy/MM/dd HH:mm")
							+ "\" WHERE uuid = \"" + p.getUniqueId() + "\""
					);
					plugin.getLogger().info("Updated user : "
						+ p.getName() + " ("
						+ p.getUniqueId() + ")");

					String msg = core.getMessage("Message.Event.Join")
						.replaceAll("%player%", p.getName());
					event.setJoinMessage(msg);
				} catch (SQLException e) {
					plugin.getLogger().warning("Error! Failed updating player...");
					p.kickPlayer(core.getMessage("Error.Messages.Join"));
					e.printStackTrace();
					return;
				}
			}
			
			AuthCode.noticeAuth(p);
		} catch (SQLException e) {
			plugin.getLogger().warning("Error! Failed checking player...");
			p.kickPlayer(core.getMessage("Error.Messages.UnknownJoin"));
			e.printStackTrace();
			return;
		}
	}
	
	/**
	 * 指定されたプレイヤーが初ログインか確認します。
	 * 
	 * @param p 確認するプレイヤー
	 * 
	 * @return 初ログインであればtrue、そうでなければfalse
	 * 
	 * @throws SQLException
	 */
	boolean isNewPlayer(Player p) throws SQLException {
		try (Statement stmt = sql.getStatement();
			 ResultSet result = stmt.executeQuery(
				"SELECT * FROM " + CoreManager.playerTable
					+ " WHERE uuid = \"" + p.getUniqueId() + "\""
			 ))
		{
			Boolean isNew = result.next(); // 次の（最初の）行があるかを確認し、そのまま変数に格納
			// プレイヤーが存在すればfalse、存在しない（新プレイヤー）ならtrueを返す
			return (isNew) ? false : true;
		}
	}
}
